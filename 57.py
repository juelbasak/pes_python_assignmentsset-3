try:
    if a > 3:
        print(a)
except NameError as exp:
    print('Name Error : ', exp)

try:
    div = 7 / 0
    print(div)
except ZeroDivisionError as exp:
    print('Cannot Divide by Zero : ', exp)

try:
    print('a' + 3)
except TypeError as exp:
    print('Type Error : ', exp)

try:
    import NotPresent
except ModuleNotFoundError as exp:
    print('Module is not present : ', exp)


try:
    data = 'abc' ** 4
except Exception as exp:
    print('Some Error! : ', exp)
