import sys

def fib(n):
    a = 0
    b = 1
    
    new_list = []
    
    for i in range(n):
        new_list.append(a)
        
        c = a + b
        a = b
        b = c
    
    return new_list


myfib = fib(int(sys.argv[1]))

for i in myfib:
    print(i, end= ', ')

print()



