string = input('Enter a string : ')
    
reverse_string = ''
 
for i in reversed(string):
    reverse_string += i
    
if string == reverse_string:
    print('Palindrome')
else:
    print('Not Palindrome')
    
    