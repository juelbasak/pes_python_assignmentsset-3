my_string = input('Enter a string : ')

for i in my_string:
    print(i, end = '\t')

part_1 = my_string[:len(my_string) // 2]
part_2 = my_string[len(my_string) // 2:]

print('\n1st Part : ', part_1)
print('2nd Part : ', part_2)

print('Repeating 100 times : ', my_string * 100)

string_1 = input('Enter a string : ')
string_2 = input('Enter another string : ')

print('New string : ', string_1 + string_2)

