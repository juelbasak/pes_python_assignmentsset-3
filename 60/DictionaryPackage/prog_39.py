import time , calendar

current_time = time.strftime('%H:%M:%S',time.localtime())
current_date = time.strftime('%d|%m|%Y',time.localtime())

current_month = int(time.strftime('%m',time.localtime()))
current_year = int(time.strftime('%Y',time.localtime()))

if current_month < 10:
    current_month = current_month % 10

print('Current Date : ', current_date)
print('Current Time : ', current_time)

print(calendar.month(current_year, current_month))


