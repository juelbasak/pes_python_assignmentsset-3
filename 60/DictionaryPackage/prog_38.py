dict1 ={'Name':'Ramakrishna','Age':25}
dict2={'EmpId':1234,'Salary':5000}

dict1.update(dict2)
print(dict1)

dict1['Salary'] *=  0.10
dict1['Age'] = 26
dict1['Grade'] = 'B1'

print('Values : ', list(dict1.values()))
print('Keys : ', list(dict1.keys()))


del dict1['Age']
for key, value in dict1.items():
    print(key, ' : ', value)

