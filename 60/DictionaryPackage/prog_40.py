import time

for i in range(12):
    print(f'{i*5} secs passed! ',time.strftime('%H:%M:%S', time.localtime()), end = '')
    
    print('\tWaiting', end='')
    for i in range(5):
        print('.', end = '')
        time.sleep(1)
    print()
print('60 secs passed!')

t1 = time.perf_counter_ns()

import prog_32

t2 = time.perf_counter_ns()

print('Time taken : %.2f nanosecond' % (t2 - t1))

