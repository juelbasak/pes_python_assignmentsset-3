try:
    print('while True <-- ', end='')
    while True:
        pass

except KeyboardInterrupt:
    print('I happened due to Keyboard Interrupt!')
else:
    print('No Keyboard Interrupt Error')

try:
    print('if k == True <-- ', end='')
    if k == True:
        pass

except NameError:
    print('I am Name Error!')
else:
    print('No Name Error')

try:
    print('div = 7 / 0 <-- ', end='')
    div = 7 / 0

except ArithmeticError:
    print('I am Arithmetic Error!')
else:
    print('No Arithmetic Error')

try:
    print('div = 7 / 1 <-- ', end='')
    div = 7 / 1

except ArithmeticError:
    print('I am Arithmetic Error!')
else:
    print('No Arithmetic Error')
